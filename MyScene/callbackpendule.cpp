#include "callbackpendule.h"
#include "time.h"

/*void PenduleCallback::operator()( osg::Node* node, osg::NodeVisitor* nv ) {
	
			std::cerr<<"erreur3";
			osg::ref_ptr<PenduleData> data = dynamic_cast<PenduleData*> (node->getUserData() );
			osg::ref_ptr<osg::PositionAttitudeTransform> pat = static_cast<osg::PositionAttitudeTransform*> (node);
			
			double angle = osg::PI_2 * cos( sqrt( 9.14 / 60 ) * time(0) );
			std::cerr<<"erreur4";
			pat->setAttitude(osg::Quat(angle,osg::Vec3f(0,1,0)));
			traverse(node, nv); 
}*/

void PenduleCallback::operator()( osg::Node* node, osg::NodeVisitor* nv ) {
			_count++;
			osg::ref_ptr<osg::PositionAttitudeTransform> pat = static_cast<osg::PositionAttitudeTransform*> (node);
			
			double angle = osg::PI_2 * cos( sqrt( 9.14 / 70 ) * _count /60 );
			std::cout<<angle<<std::endl;
			pat->setAttitude(osg::Quat(angle,osg::Vec3f(0,1,0), 0.4, osg::Vec3f(0,0,1), 0, osg::Vec3f(1,0,0)));
			traverse(node, nv); 
}