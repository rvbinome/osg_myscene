// base

#include <osgViewer/Viewer>
#include <osg/ShapeDrawable>
#include <osg/PositionAttitudeTransform>
#include <osg/Material>
#include <osg/StateSet>
#include <osg/MatrixTransform>
#include <osg/Geometry>
#include <osgDB/ReadFile>
#include <osg/fog>
// Keyboard input
#include <osgViewer/ViewerEventHandlers>
#include <osgGA/StateSetManipulator>

#include <iostream>

#include "dataquille.h"
#include "callbackpendule.h"
#include "callbackquille.h"




int main()
{
/* OBJECTS CREATION */

	//Creating the viewer	
	osgViewer::Viewer viewer ;

	//Creating the root node
	osg::ref_ptr<osg::Group> root (new osg::Group);

	// StateSet de root
	osg::ref_ptr<osg::StateSet> rootStateSet ( root->getOrCreateStateSet() );

/* TERRAIN */
	
	osg::ref_ptr<osg::PositionAttitudeTransform> translationTerrain ( new osg::PositionAttitudeTransform);
	
	translationTerrain->setPosition(osg::Vec3f(37,130,22));
	// Create transformation node
	osg::ref_ptr<osg::MatrixTransform> terrainScaleMAT (new osg::MatrixTransform);

	// Scale matrix
	osg::Matrix terrainScaleMatrix;
	terrainScaleMatrix.makeScale(osg::Vec3f(1,1,1));

	//Loading the terrain node
	osg::ref_ptr<osg::Node> terrainnode (osgDB::readNodeFile("scene.3ds"));
	//Set transformation node parameters
	terrainScaleMAT->addChild(terrainnode);
	terrainScaleMAT->setMatrix(terrainScaleMatrix);

	translationTerrain->addChild(terrainScaleMAT.get());

/* PENDULE */

	osg::ref_ptr<osg::PositionAttitudeTransform> translationHaute ( new osg::PositionAttitudeTransform);
	
	osg::ref_ptr<osg::PositionAttitudeTransform> translationBoulle ( new osg::PositionAttitudeTransform);
   	osg::ref_ptr<osg::Geode> boulleGeode (new osg::Geode);
	osg::ref_ptr<osg::Sphere> myBoulle (new osg::Sphere(osg::Vec3f(),5));
	osg::ref_ptr<osg::ShapeDrawable> boulleDrawable (new osg::ShapeDrawable(myBoulle.get()));
	boulleGeode->addDrawable(boulleDrawable.get());
	

	
   	osg::ref_ptr<osg::Geode> filGeode (new osg::Geode);
	osg::ref_ptr<osg::Cylinder> myFil (new osg::Cylinder(osg::Vec3f(0,0,-35),0.5,70));
	osg::ref_ptr<osg::ShapeDrawable> FilDrawable (new osg::ShapeDrawable(myFil.get()));
	filGeode->addDrawable(FilDrawable.get());

	
	translationHaute->setPosition(osg::Vec3f(0,0,10));
	translationHaute->setAttitude(osg::Quat(osg::PI_2,osg::Vec3f(0,1,0), 0.4, osg::Vec3f(0,0,1), 0, osg::Vec3f(1,0,0)));

	translationHaute->addChild(translationBoulle.get());
	translationHaute->addChild(filGeode.get());

	
	translationBoulle->setPosition(osg::Vec3f(0,0,-70));
	translationBoulle->addChild(boulleGeode.get());

/* mise en place du callback pour le pendule*/
	translationHaute->setUpdateCallback(new PenduleCallback() );
	
// Quille
	//BOX du pendule
	const osg::BoundingSphere& boxPendule = boulleGeode->getBound();
	std::cout<<boxPendule.radius()<<std::endl;
	
	osg::ref_ptr<osg::Geode> quilleGeode (new osg::Geode);
	osg::ref_ptr<osg::Cylinder> myQuille (new osg::Cylinder(osg::Vec3f(0,0,0),5,20));
	osg::ref_ptr<osg::ShapeDrawable> quilleDrawable (new osg::ShapeDrawable(myQuille.get()));
	quilleGeode->addDrawable(quilleDrawable.get());
	
	osg::ref_ptr<osg::PositionAttitudeTransform> translationQuille ( new osg::PositionAttitudeTransform);
	
	translationQuille->setPosition(osg::Vec3f(0,0,-60));
	translationQuille->addChild(quilleGeode.get());

	const osg::BoundingSphere& boxQuille =  translationQuille->getBound();
	
	std::cout<<boxQuille.radius()<<std::endl;



	
	//QuilleData* data = new QuilleData(boxPendule, boxQuille);
	//QuilleData* data = new QuilleData(boulleGeode.get(), translationQuille.get());
	QuilleData* data = new QuilleData();
	//Callback pour la quille
	translationQuille->setUserData(data);
	translationQuille->setUpdateCallback(new QuilleCallback() );
	




/* LIGHTING */
	
	//Create nodes

	osg::ref_ptr<osg::Group> lightGroup1 (new osg::Group);
	osg::ref_ptr<osg::LightSource> lightSource1 = new osg::LightSource;
	osg::ref_ptr<osg::LightSource> lightSource2 = new osg::LightSource;

	//Create a local light
	osg::Vec4f lightPosition1(-5,-5,0,0);
	osg::Vec4f lightPosition2(5,5,0,0);

	osg::ref_ptr<osg::Light> myLight1 = new osg::Light;
	myLight1->setLightNum(0);
	myLight1->setPosition(lightPosition1);
	myLight1->setDiffuse(osg::Vec4f(1,0,0,0));
	myLight1->setSpecular(osg::Vec4f(1,0,0,0));
	myLight1->setConstantAttenuation(0.1);
	
	osg::ref_ptr<osg::Light> myLight2 = new osg::Light;
	myLight2->setLightNum(1);
	myLight2->setPosition(lightPosition2);
	myLight2->setDiffuse(osg::Vec4f(0,0,1,0));
	myLight2->setSpecular(osg::Vec4f(0,0,1,0));
	myLight2->setConstantAttenuation(0.1);

	//Set light source parameters
	lightSource1->setLight(myLight1.get());
	lightSource2->setLight(myLight2.get());

	//Add to light source group
	lightGroup1->addChild(lightSource1.get());
	lightGroup1->addChild(lightSource2.get());

	// Allumage des lumi�res
	rootStateSet->setMode(GL_LIGHT0, osg::StateAttribute::ON);
	rootStateSet->setMode(GL_LIGHT1, osg::StateAttribute::ON);
	

/* KEYBOARD INPUT */
	
 	//Stats Event Handler s key
	viewer.addEventHandler(new osgViewer::StatsHandler);

	//Windows size handler
	viewer.addEventHandler(new osgViewer::WindowSizeHandler);
	
	// add the state manipulator
    viewer.addEventHandler( new osgGA::StateSetManipulator(viewer.getCamera()->getOrCreateStateSet()) );

		
/* SCENE GRAPH*/
	root->addChild(translationHaute.get());
	root->addChild(translationQuille.get());
	root->addChild(lightGroup1.get());
	root->addChild(translationTerrain.get());




	// Set the scene data
	viewer.setSceneData( root.get() ); 

/* START VIEWER */

	//The viewer.run() method starts the threads and the traversals.
	return (viewer.run());
}
