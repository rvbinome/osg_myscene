#include "callbackquille.h"
#include "dataquille.h"

void QuilleCallback::operator()( osg::Node* node, osg::NodeVisitor* nv ) {
			_count++;
			osg::ref_ptr<QuilleData> data = dynamic_cast<QuilleData*> (node->getUserData() );
			osg::ref_ptr<osg::PositionAttitudeTransform> pat = static_cast<osg::PositionAttitudeTransform*> (node);
			
			if ( data->perc == false ) {
				
				if ( osg::PI_2 * cos( sqrt( 9.14 / 70 ) * _count /60 ) < 0.2  ) {

					data->acceleration = osg::Vec3f(150,70,200);
					data->perc = true;
				}

				/*if ( data->getPendule().intersects(data->getQuille()) ) {
						pat->setPosition(osg::Vec3f(0,0,-80));
						data->perc = true;
				}*/
				/*if ( data->boulle->getBound().intersects(data->quille->getBound()) ) {
						pat->setPosition(osg::Vec3f(0,0,-80));
						data->perc = true;
				}*/
			}else {

				data->acceleration += osg::Vec3f( 0,0,-0.05*10*9.10);
				data->vitesse += data->acceleration / 60;
				data->position += data->vitesse / 60;

				
				pat->setPosition(data->position);
			}


			traverse(node, nv); 
}