#pragma once


#include <osgViewer/Viewer>
#include <osg/ShapeDrawable>
#include <osg/PositionAttitudeTransform>
#include <osg/Material>
#include <osg/StateSet>
#include <osg/MatrixTransform>
#include <osg/Geometry>
#include <osgDB/ReadFile>
#include <osg/fog>
// Keyboard input
#include <osgViewer/ViewerEventHandlers>
#include <osgGA/StateSetManipulator>

#include <iostream>


class QuilleData : public osg::Referenced {
	public:
	//	QuilleData(const osg::BoundingSphere& pendule, const osg::BoundingSphere& quille);
	//	QuilleData(osg::Geode* _boulle, osg::PositionAttitudeTransform* _quille);
		QuilleData();
		bool perc;
       osg::Vec3 position;
       osg::Vec3 vitesse; 
       osg::Vec3 acceleration;
	 //  osg::ref_ptr<osg::PositionAttitudeTransform> quille;
	 //  osg::ref_ptr<osg::Geode> boulle;
	   //const osg::BoundingSphere getPendule () const { return penduleBox; }
	  // const osg::BoundingSphere getQuille () const { return quilleBox; }
	private:
	   //const osg::BoundingSphere& penduleBox;
	   //const osg::BoundingSphere& quilleBox;
}; 

