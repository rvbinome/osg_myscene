#pragma once


#include <osgViewer/Viewer>
#include <osg/ShapeDrawable>
#include <osg/PositionAttitudeTransform>
#include <osg/Material>
#include <osg/StateSet>
#include <osg/MatrixTransform>
#include <osg/Geometry>
#include <osgDB/ReadFile>
#include <osg/fog>
// Keyboard input
#include <osgViewer/ViewerEventHandlers>
#include <osgGA/StateSetManipulator>

#include <iostream>


class PenduleCallback : public osg::NodeCallback {
public:
	PenduleCallback() : _count(0) {};

		virtual void operator()( osg::Node* node, osg::NodeVisitor* nv );
protected:
	unsigned int _count;

};